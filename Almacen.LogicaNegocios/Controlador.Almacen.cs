﻿using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using Almacen.AccesoDatos;
using Almacen.Entidades;

namespace Almacen.LogicaNegocios
{
    public class cAlmacen
    {
        private Datos GetInfo;
        public cAlmacen()
        {
            GetInfo = new Datos();
        }
        public bool VerificarPersonal(string user,string pass)
        {
            bool result = GetInfo.ValidarIdentidad(user, pass);
            return result;
        }
        public List<Horario> GetHorarios()
        {
            List<Horario> result = new List<Horario>();
            int a = 7;
            for (int i = 0; i < 15; i++)
            {
                Horario H = new Horario();
                H.Hora = string.Format("{0}:00 - ",a);
                a++;
                H.Hora += string.Format("{0}:00", a);
                result.Add(H);
            }
            return result;
        }
        public List<Herramienta> MostrarInventario(Herramienta Herramienta, int opcion)
        {
            var mostrar = GetInfo.VerInventario(Herramienta, opcion);
            return mostrar;
        }
        public List<PrestamoHerramienta> MostrarHistoria(PrestamoHerramienta Prestamo, int opcion)
        {
            var mostrar = GetInfo.VerHistoria(Prestamo, opcion);
            return mostrar;
        }
        public List<Materia> Combos(string cadena)
        {
            var mostrar = GetInfo.VerComboBox(cadena);
            return mostrar;
        }
        public void SetPrestamo(PrestamoHerramienta Prestamo, int opcion)
        {
            GetInfo.SetEntregado(Prestamo,opcion);
        }
        public bool VerificarDisponibilidadHerramienta(string code)
        {
            int result = GetInfo.VerificarExistencia(code);
            if (result == 1)
                return true;
            else
                return false;
        }
        public List<Persona> GetPersona(Persona Persona,int opcion)
        {
            var result = GetInfo.VerPErsonas(Persona, opcion);
            return result;
        }
        public bool AgregarMateria(string materia)
        {
            int result = GetInfo.AgregarMateria(materia);
            if (result != -1)
                return true;
            else
                return false;
        }
        public bool AgregarAula(string Aula)
        {
            int result = GetInfo.AgregarAula(Aula);
            if (result != -1)
                return true;
            else
                return false;
        }
        public bool AgreagarPersonas(Persona persona)
        {
            int result = GetInfo.AgregarPersonas(persona);
            if (result != -1)
                return true;
            else
                return false;
        }
        public void PrestarHerramienta(PrestamoHerramientaDetalle Prestamo,int opcion)
        {
            GetInfo.PrestarHerramientas(Prestamo, opcion);
        }
        public List<PrestamoHerramientaDetalle> GetDetalle(int prestamo)
        {
            var result = GetInfo.GetDetalle(prestamo);
            return result;
        }
        public List<PrestamoLaboratorio> GetPrestamoLaboratorio(int aula)
        {
            var result = GetInfo.GetPrestamoLaboratorio(aula);
            return result;
        }
        public List<Horario> GetHorarios(List<PrestamoLaboratorio> filtro)
        {
            List<Horario> result = new List<Horario>();
            int a = 7;
            for (int i = 0; i < 15; i++)
            {
                Horario H = new Horario();
                H.Hora = string.Format("{0}:00 - ", a);
                try
                {
                    for (int p = 0; p < filtro.Count; p++)
                    {
                        switch (filtro[p].Dia)
                        {
                            case 1:
                                if (a == int.Parse(filtro[p].Entrada.Substring(0, 2))&& a+1 == int.Parse(filtro[p].Salida.Substring(0, 2)))
                                    H.Lunes = filtro[p].Responsable;
                                break;
                            case 2:
                                if (a == int.Parse(filtro[p].Entrada.Substring(0, 2)) && a+1 == int.Parse(filtro[p].Salida.Substring(0, 2)))
                                    H.Martes = filtro[p].Responsable;
                                break;
                            case 3:
                                if (a == int.Parse(filtro[p].Entrada.Substring(0, 2)) && a+1 == int.Parse(filtro[p].Salida.Substring(0, 2)))
                                    H.Miercoles = filtro[p].Responsable;
                                break;
                            case 4:
                                if (a == int.Parse(filtro[p].Entrada.Substring(0, 2)) && a+1 == int.Parse(filtro[p].Salida.Substring(0, 2)))
                                    H.Jueves = filtro[p].Responsable;
                                break;
                            case 5:
                                if (a == int.Parse(filtro[p].Entrada.Substring(0, 2)) && a+1 == int.Parse(filtro[p].Salida.Substring(0, 2)))
                                    H.Viernes = filtro[p].Responsable;
                                break;
                            case 6:
                                if (a == int.Parse(filtro[p].Entrada.Substring(0, 2)) && a+1 == int.Parse(filtro[p].Salida.Substring(0, 2)))
                                    H.Sabado = filtro[p].Responsable;
                                break;
                            default:
                                break;
                        }
                    }
                }
                catch (Exception) { }
                a++;
                H.Hora += string.Format("{0}:00", a);
                result.Add(H);
            }
            return result;
        }
        public bool CambiarContrasena(string pass)
        {
            bool result = GetInfo.CambiarPasswordAdmin("",pass);
            return result;
        }
        public Configuraciones GetConfiguracion()
        {
            Configuraciones result = GetInfo.GetConfiguraciones();
            return result;
        }
        public string MostrarContrasena()
        {
            string result = GetInfo.MostrarContrasena();
            return result;
        }
        public bool SetConfiguracion(bool Confirmacion,bool Verificacion,bool Errores)
        {
            bool result = GetInfo.SetConfiguracion(Confirmacion, Verificacion, Errores);
            return result;
        }
        public bool SetHerramientas(Herramienta herramienta)
        {
            bool result = GetInfo.AgregarHerramientas(herramienta);
            return result;
        }
        public bool SetPrestamoLaboratorio(PrestamoLaboratorio Laboratorio,int op)
        {
            bool result = GetInfo.SetPrestamoLaboratorio(Laboratorio, op);
            return result;
        }
    }
}
