drop database if exists AlmacenABMODEL;
-- Alpha Buena Marabilla Onda Dinamita Escuadron Lobo
create database AlmacenABMODEL;
use AlmacenABMODEL;

#region Tablas finales
create table herramientas(
  CodigoQR varchar(200) primary key,
  NombreHerramienta varchar(50),
  Prioridad varchar(10),
  Descripcion varchar(100),
  Estatus varchar(20),
  FechaRegistro date,
  Disponibilidad bool
);

drop procedure if exists pHerramientas;
create procedure pHerramientas(
  in _CodigoQR varchar(200),
  in _NombreHerramienta varchar(50),
  in _Prioridad varchar(10),
  in _Descripcion varchar(100),
  in _Estatus varchar(20),
  in _FechaRegistro date,
  in opcion int
)begin
  if opcion = 1 then
    select CodigoQR,NombreHerramienta,Prioridad,Descripcion,Estatus,DATE_FORMAT(FechaRegistro,'%Y/%m/%d') as 'FechaRegistro',disponibilidad from Herramientas where NombreHerramienta like CONCAT('%',_NombreHerramienta,'%');
  else if opcion = 2 then
    insert into Herramientas values(_CodigoQR,_NombreHerramienta,_Prioridad,_Descripcion,_Estatus,_FechaRegistro,true);
  else if opcion = 3 then
    select * from Herramientas where NombreHerramienta Like CONCAT('%',_NombreHerramienta,'%') and Disponibilidad = true;
  else if opcion = 4 then
    select count(*) as 'existe' from Herramientas where CodigoQR = _CodigoQR;
  end if;end if;end if; end if;
end;

create table Personas(
  Control varchar(18) primary key,
  Nombres varchar(60) not null,
  Paterno varchar(40),
  Materno varchar(40)
);

drop procedure if exists pPersonas;
create procedure pPersonas(
  in _Control varchar(18),
  in _Nombres varchar(60),
  in _Paterno varchar(40),
  in _Materno varchar(40),
  in opcion int
)begin
  declare existe int default 0;
  select count(*) from Personas where Control = _Control into existe;
  if opcion = 1 then
    if existe = 0 then
      insert into Personas values(_Control,_Nombres,_Paterno,_Materno);
      set existe = 1;
    end if;
    select existe;
  else if opcion = 2 then
    select existe as "existe";
  else if opcion = 3 then
    select * from Personas where Control like CONCAT('%',_Control,'%');
  end if;end if;end if;
end;

create table Materias(
  ID int not null auto_increment primary key,
  Nombre varchar(90)
);

drop procedure if exists pMaterias;
create procedure pMaterias(
  in _Materia varchar(90),
  in opcion int
)begin
  declare existe int default 0;
  if opcion = 1 then
    select * from Materias;
  else if opcion = 2 then
    select count(*) from Materias where Nombre = _Materia into existe;
    if existe = 0 then
      insert into Materias values(null,_Materia);
      set existe = '1';
    end if;
    select existe;
  end if;end if;
end;

create table Aulas(
  ID int not null auto_increment primary key,
  Nombre varchar(100)
);

drop procedure if exists pAulas;
create procedure pAulas(
  in _Aula varchar(100),
  in opcion int
)begin
  declare existe int default 0;
  if opcion = 1 then
    select * from Aulas where Nombre not like '%lab%';
  else if opcion = 2 then
    select * from Aulas where Nombre like '%lab%';
  else if opcion = 3 then
    select * from Aulas;
  else if opcion = 4 then
    select count(*) from aulas where nombre = _Aula into existe;
    if existe = 0 then
      insert into Aulas values(null,_Aula);
      set existe = '1';
    end if;
    select existe;
  else if opcion = 5 then
    select * from aulas;
  end if;end if;end if;end if; end if;
end;
call pAulas("", 5);

create table PrestamoHerramientas(
  ID int not null auto_increment primary key,
  FK_Persona varchar(18) not null,
  Fecha date,
  Hora time,
  Nota text,
  Status bool,
  FK_Aula int,
  FK_Materia int,
  foreign key(FK_Persona)references Personas(Control),
  foreign key(FK_Aula)references Aulas(ID),
  foreign key(FK_Materia)references Materias(ID)
);

drop procedure if exists pPrestamoH;
create procedure pPrestamoH(
  in _ID int,
  in _FKPersona varchar(18),
  in _Fecha date,
  in _Hora time,
  in _Nota text,
  in _FKAula int,
  in _FKMateria int,
  in opcion int
)begin
  if opcion = 1 then
    SET FOREIGN_KEY_CHECKS=0;
    insert into PrestamoHerramientas values(null,_FKPersona,_Fecha,_Hora,_Nota,false,_FKAula,_FKMateria);
    SET FOREIGN_KEY_CHECKS=1;
  else if opcion = 2 then
    update PrestamoHerramientas set Nota = _Nota where ID = _ID;
  else if opcion = 3 then
    update PrestamoHerramientas set Status = true where ID = _ID;
  else if opcion = 4 then
    select PrestamoHerramientas.ID as "ID",FK_Persona,DATE_FORMAT(Fecha,"%Y/%m/%d") as "Fecha",TIME_FORMAT(Hora,"%H:%i")as "Hora",Nota,Status,Aulas.Nombre as "FK_Aula",Materias.Nombre as "FK_Materia" from PrestamoHerramientas,Aulas,Materias where FK_Persona Like CONCAT('%',_FKPersona,'%') and Status = false and FK_Aula = Aulas.ID and FK_Materia = Materias.ID order by prestamoherramientas.Fecha desc,prestamoherramientas.Hora desc;
  else if opcion = 5 then
    select PrestamoHerramientas.ID as "ID",FK_Persona,DATE_FORMAT(Fecha,"%Y/%m/%d") as "Fecha",TIME_FORMAT(Hora,"%H:%i")as "Hora",Nota,Status,Aulas.Nombre as "FK_Aula",Materias.Nombre as "FK_Materia" from PrestamoHerramientas,Aulas,Materias where FK_Persona Like CONCAT('%',_FKPersona,'%') and Status = true and FK_Aula = Aulas.ID and FK_Materia = Materias.ID order by prestamoherramientas.Fecha desc,prestamoherramientas.Hora desc;
  end if;end if;end if;end if;end if;
end;
select PrestamoHerramientas.ID as "ID",FK_Persona,DATE_FORMAT(Fecha,"%Y/%m/%d") as "Fecha",TIME_FORMAT(Hora,"%H:%i")as "Hora",Nota,Status,Aulas.Nombre as "FK_Aula",Materias.Nombre as "FK_Materia" from PrestamoHerramientas,Aulas,Materias where FK_Persona Like CONCAT('%',"",'%') and Status = true and FK_Aula = Aulas.ID and FK_Materia = Materias.ID order by prestamoherramientas.Fecha desc,prestamoherramientas.Hora desc;
create table PrestamoDetalle(
  ID int auto_increment primary key not null,
  FK_Prestamo int,
  FK_Herramienta varchar(200),
  foreign key(FK_Prestamo)references PrestamoHerramientas(ID),
  foreign key(FK_Herramienta)references Herramientas(CodigoQR)
);

drop procedure if exists pPrestamoD;
create procedure pPrestamoD(
  in _FKPrestamo int,
  in _FKHerramienta varchar(200),
  in opcion int
)begin
  declare prestamo int;
  if opcion = 1 then
    select MAX(id) from prestamoherramientas into prestamo;
    insert into PrestamoDetalle values(null,prestamo,_FKHerramienta);
    update Herramientas set Disponibilidad = false where CodigoQR = _FKHerramienta;
  else if opcion = 2 then
    select FK_Prestamo,FK_Herramienta,NombreHerramienta,Descripcion from PrestamoDetalle,herramientas where FK_Prestamo = _FKPrestamo and FK_Herramienta = CodigoQR;
  else if opcion = 3 then
    update Herramientas set Disponibilidad = true where CodigoQR = _FKHerramienta;
  end if;end if;end if;
end;

create table PrestamoAula(
  ID int not null auto_increment primary key,
  FK_Aula int,
  Dia int,
  HoraEntrada time,
  HoraSalida time,
  FK_Responsable varchar(18),
  foreign key(FK_Aula)references Aulas(ID),
  foreign key(FK_Responsable)references Personas(Control)
);

drop procedure if exists pPrestamoA;
create procedure pPrestamoA(
  in _ID int,
  in _FKAula int,
  in _Dia int,
  in _HoraEntrada time,
  in _HoraSalida time,
  in _FKResponsable varchar(18),
  in opcion int
)begin
  declare existe int default 0;
  if opcion = 1 then
    select ID from PrestamoAula where _Dia = Dia and FK_Aula = _FKAula and Time_Format(_HoraEntrada,"%H") = Time_Format(HoraEntrada,"%H") into existe;
    if existe = 0 then
      insert into PrestamoAula values(null,_FKAula,_Dia,_HoraEntrada,_HoraSalida,_FKResponsable);
    end if;
  else if opcion = 2 then
    update PrestamoAula set FK_Aula = _FKAula,Dia = _Dia,HoraEntrada = _HoraEntrada,HoraSalida = _HoraSalida,FK_Responsable = _FKResponsable where ID = _ID;
  else if opcion = 3 then
    select * from PrestamoAula where FK_Aula = _FKAula;
  else if opcion = 4 then
    delete from PrestamoAula where _Dia = Dia and FK_Aula = _FKAula and Time_Format(_HoraEntrada,"%H") = Time_Format(HoraEntrada,"%H");
  end if;end if;end if;end if;
end;

create table usuario(
  nombreUsuario varchar(100) primary key,
  contrasenia varchar(10)
);

insert into usuario values('Admon','1234');
drop procedure if exists pUsuario;
create procedure pUsuario(
  in _Nombre varchar(100),
  in _Pass varchar(10),
  in opcion int
)begin
  if opcion = 1 then
    select count(*) as "true" from Usuario where nombreUsuario = _Nombre and contrasenia = _Pass;
  else if opcion = 2 then
    update Usuario set contrasenia = _Pass;
  else if opcion = 3 then
    select contrasenia from Usuario;
  end if;end if;end if;
end;

drop table if exists Configuracion;
create table Configuracion(
  ID int not null auto_increment primary key,
  Confirmacion bool,
  Verificacion bool,
  Error bool
);

insert into Configuracion values(null,true,true,true);
drop procedure if exists pConfiguracion;
create procedure pConfiguracion(
  in _Conf bool,
  in _Veri bool,
  in _Erro bool,
  in opcion int
)begin
  if opcion = 1 then
    update Configuracion set Confirmacion = _Conf, Verificacion = _Veri,Error = _Erro;
  else if opcion = 2 then
    select * from Configuracion;
  end if;end if;
end;

#end region
call pConfiguracion(false,false,false,2);


insert into herramientas values('codigoqr','desarmador','prioridad','desarmador de cruz rojo','estatus','2020-06-18',true),
('codigo qr','desarmador','prioridad','desarmador de estrella','estatus','2020-06-18',true),
('qrcode','martillo','prioridad','martillote','estatus','2020-06-18',true);