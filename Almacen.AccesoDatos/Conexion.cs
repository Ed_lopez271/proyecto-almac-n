﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace Almacen.AccesoDatos
{
    class Conexion
    {
        MySqlConnection _conn;

        public Conexion(string Server, string User, string Pass, string DB, uint Port)
        {
            MySqlConnectionStringBuilder cadena = new MySqlConnectionStringBuilder();
            cadena.Server = Server;
            cadena.UserID = User;
            cadena.Password = Pass;
            cadena.Database = DB;
            cadena.Port = Port;
            _conn = new MySqlConnection(cadena.ToString());
        }
        public string Ejecutar(string Cadena)
        {
            try
            {
                _conn.Open();
                MySqlCommand Cmm = new MySqlCommand(Cadena, _conn);
                Cmm.ExecuteNonQuery();
                _conn.Close();
                return "true";
            }
            catch (Exception e)
            {
                _conn.Close();
                return e.Message;
            }
        }
        public DataSet ObtenerDatos(String Cadena)
        {
            try
            {
                var ds = new DataSet();
                MySqlDataAdapter DA = new MySqlDataAdapter(Cadena, _conn);
                DA.Fill(ds);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
