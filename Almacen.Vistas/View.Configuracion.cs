﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Almacen.Entidades;
using Almacen.LogicaNegocios;

namespace Almacen.Vistas
{
    public partial class frmConfiguracion : Form
    {
        private Configuraciones Myconfig;
        private cAlmacen ControlAlmacen;
        private frmAgregarHerramienta AH;
        public frmConfiguracion(Configuraciones Configuracion)
        {
            InitializeComponent();
            ControlAlmacen = new cAlmacen();
            this.Myconfig = Configuracion;
            Myconfig.contraseña = ControlAlmacen.MostrarContrasena();
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            if(txtContraseña.PasswordChar == '*')
                txtContraseña.PasswordChar = Char.Parse(Char.ConvertFromUtf32(0));
            else
                txtContraseña.PasswordChar = '*';
        }

        private void frmConfiguracion_Load(object sender, EventArgs e)
        {
            Llenar();
        }

        private void btnAceptarCambios_Click(object sender, EventArgs e)
        {
            Rellenar();
            frmMensaje M = new frmMensaje("Verificar", "¿Esta seguro de realizar estos cambios?");
            DialogResult R = M.ShowDialog();
            if (R == DialogResult.OK)
            {
                if (ControlAlmacen.SetConfiguracion(Myconfig.Confirmacion, Myconfig.Verificacion, Myconfig.Error))
                {
                    if (ControlAlmacen.CambiarContrasena(Myconfig.contraseña))
                    {
                        frmMensaje Re = new frmMensaje("Correcto", "Todos los cambios se realizaron correctamente");
                        Re.ShowDialog();
                    }
                    else
                        error();
                }
                else error();
            }
        }
        private void error()
        {
            frmMensaje Er = new frmMensaje("Error", "Los cambios no se realizaron correctamente,\nfavor de volver a intentar");
            Er.ShowDialog();
        }
        private void Llenar()
        {
            chbVerificacion.Checked = Myconfig.Verificacion;
            chbConfirmacion.Checked = Myconfig.Confirmacion;
            chbError.Checked = Myconfig.Error;
            txtContraseña.Text = Myconfig.contraseña;
        }
        private void Rellenar()
        {
            Myconfig.Verificacion = chbVerificacion.Checked;
            Myconfig.Confirmacion = chbConfirmacion.Checked;
            Myconfig.Error = chbError.Checked;
            Myconfig.contraseña = txtContraseña.Text;
        }

        private void btnAgregarInventario_Click(object sender, EventArgs e)
        {
            if (AH == null)
            {
                AH = new frmAgregarHerramienta();
                AH.Show();
            }
            else if (AH.IsDisposed)
            {
                AH = new frmAgregarHerramienta();
                AH.Show();
            }
            else
                AH.Focus();
        }
    }
}
