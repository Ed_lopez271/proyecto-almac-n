﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Almacen.Entidades;
using Almacen.LogicaNegocios;

namespace Almacen.Vistas
{
    public partial class frmInventario : Form
    {
        private bool Inventario;
        private bool selectar = false;
        private cAlmacen GetHistorial;
        public frmInventario(bool Historial)
        {
            InitializeComponent();
            this.Inventario = Historial;
            GetHistorial = new cAlmacen();
        }

        private void frmInventario_Load(object sender, EventArgs e)
        {
            if (this.Inventario)
                dgvInventario.DataSource = GetHistorial.MostrarInventario(new Herramienta() { Nombre = "" }, 1);
            else
            {
                dgvInventario.Size = new Size(676, 316);
                dgvInventario.DataSource = GetHistorial.MostrarHistoria(new PrestamoHerramienta() { Persona = "" }, 5);
                OnlyOne();
            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            if (this.Inventario)
            {
                dgvInventario.DataSource = null;
                dgvInventario.DataSource = GetHistorial.MostrarInventario(new Herramienta() { Nombre = txtFiltro.Text }, 1);
            }
            else
            {
                dgvInventario.DataSource = null;
                dgvInventario.DataSource = GetHistorial.MostrarHistoria(new PrestamoHerramienta() { Persona = txtFiltro.Text }, 5);
                OnlyOne();
            }
        }

        private void dgvInventario_SelectionChanged(object sender, EventArgs e)
        {
            if(selectar && !Inventario)
            {
                var algo = dgvInventario.SelectedRows[0];
                int _id = int.Parse(algo.Cells[0].Value.ToString());
                dgvDetalle.DataSource = null;
                dgvDetalle.DataSource = GetHistorial.GetDetalle(_id);
            }
        }

        private void frmInventario_Shown(object sender, EventArgs e)
        {
            selectar = true;
        }

        private void txtFiltro_Enter(object sender, EventArgs e)
        {
            selectar = false;
        }

        private void dgvInventario_Enter(object sender, EventArgs e)
        {
            selectar = true;
        }
        private void OnlyOne()
        {
            if (dgvInventario.Rows.Count == 1)
            {
                var algo = dgvInventario.Rows[0];
                int _id = int.Parse(algo.Cells[0].Value.ToString());
                dgvDetalle.DataSource = null;
                dgvDetalle.DataSource = GetHistorial.GetDetalle(_id);
            }
        }
    }
}
