﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Almacen.Entidades;
using Almacen.LogicaNegocios;

namespace Almacen.Vistas
{
    public partial class frmHerramienta : Form
    {
        private List<AntePrestamo> SetHerramienta;
        private cAlmacen ControlAlmacen;
        private List<Materia> materia = new List<Materia>();
        private List<Materia> aula = new List<Materia>();
        private Configuraciones MyConfig;

        public frmHerramienta(Configuraciones Configuracion)
        {
            InitializeComponent();
            SetHerramienta = new List<AntePrestamo>();
            ControlAlmacen = new cAlmacen();
            this.MyConfig = Configuracion;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (ControlAlmacen.VerificarDisponibilidadHerramienta(txtCodigo.Text))
            {
                AntePrestamo H = new AntePrestamo()
                {
                    Codigo = txtCodigo.Text,
                    Nombre = txtHerramienta.Text
                };
                SetHerramienta.Add(H);
                dgvPrestamo.DataSource = null;
                dgvPrestamo.DataSource = SetHerramienta;
                txtHerramienta.Clear();
                txtCodigo.Clear();
            }
            else
            {
                if (MyConfig.Error)
                {
                    frmMensaje M = new frmMensaje("Error", "No existe ninguna herraimenta con esas caracteristicas");
                    M.ShowDialog();
                }
            }
        }

        private void frmHerramienta_Load(object sender, EventArgs e)
        {
            materia = ControlAlmacen.Combos("call pMaterias('',1);");
            for (int i = 0; i < materia.Count; i++)
            {
                cmbMateria.Items.Add(materia[i].Nombre);
            }
            aula = ControlAlmacen.Combos("call pAulas('',5)");
            for (int i = 0; i < aula.Count; i++)
            {
                cmbAula.Items.Add(aula[i].Nombre);
            }
        }

        private void dgvPrestamo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var algo = dgvPrestamo.SelectedRows[0];
            string a = algo.Cells[0].Value.ToString();
            for (int i = 0; i < SetHerramienta.Count; i++)
            {
                if (SetHerramienta[i].Codigo == a)
                    SetHerramienta.RemoveAt(i);
            }
            dgvPrestamo.DataSource = null;
            dgvPrestamo.DataSource = SetHerramienta;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            if (MyConfig.Confirmacion)
            {
                frmMensaje M = new frmMensaje("Confirmar", "Esta seguro de cancelar el prestamo");
                var a = M.ShowDialog();
                if (a == DialogResult.OK)
                    Cancelar();
            }
            else
                Cancelar();
        }
        private void Cancelar()
        {
            SetHerramienta.Clear();
            dgvPrestamo.DataSource = null;
            dgvPrestamo.DataSource = SetHerramienta;
            Limpiar();
        }
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            List<Herramienta> result = ControlAlmacen.MostrarInventario(new Herramienta() { Nombre = txtHerramienta.Text }, 3);
            int usar = -1;
            for (int i = 0; i < result.Count; i++)
            {
                bool use = true;
                for (int p = 0; p < SetHerramienta.Count; p++)
                {
                    if (result[i].Codigo == SetHerramienta[p].Codigo)
                        use = false;
                }
                if (use)
                {
                    usar = i;
                    i = result.Count;
                }
            }
            if (usar != -1)
            {
                txtCodigo.Text = result[usar].Codigo;
                txtHerramienta.Text = result[usar].Nombre;
            }
        }
        private void txtNoControl_TextChanged(object sender, EventArgs e)
        {
            txtMaterno.Clear();
            txtPaterno.Clear();
            txtNombre.Clear();
            if (!string.IsNullOrEmpty(txtNoControl.Text))
            {
                List<Persona> result = ControlAlmacen.GetPersona(new Persona() { ID = txtNoControl.Text }, 3);
                if (result.Count > 0)
                {
                    txtNombre.Text = result[0].Nombre;
                    txtPaterno.Text = result[0].Paterno;
                    txtMaterno.Text = result[0].Materno;
                }
            }
        }
        void Limpiar()
        {
            txtCodigo.Clear();
            txtHerramienta.Clear();
            txtNoControl.Clear();
            txtNombre.Clear();
            txtPaterno.Clear();
            txtMaterno.Clear();
            cmbAula.Text = "";
            cmbMateria.Text = "";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int a = 0,m = 0;
            for (int i = 0; i < materia.Count; i++)
            {
                if (cmbMateria.Text == materia[i].Nombre)
                    m = materia[i].ID;
            }
            for (int i = 0; i < materia.Count; i++)
            {
                if (cmbAula.Text == aula[i].Nombre)
                    a = aula[i].ID;
            }

            if (SetHerramienta.Count <= 0) Mensaje();
            else if (string.IsNullOrEmpty(txtNoControl.Text)) Mensaje();
            else if (string.IsNullOrEmpty(txtNombre.Text)) Mensaje();
            else if (string.IsNullOrEmpty(txtMaterno.Text)) Mensaje();
            else if (string.IsNullOrEmpty(txtPaterno.Text)) Mensaje();
            else
            {
                if (ControlAlmacen.AgregarMateria(cmbMateria.Text))
                {
                    int m2 = 0;
                    var mat = ControlAlmacen.Combos("call pMaterias('"+cmbMateria.Text+"',1)");
                    for (int i = 0; i < mat.Count; i++)
                    {
                        if (cmbMateria.Text == mat[i].Nombre)
                            m2 = mat[i].ID;
                    }

                    if (ControlAlmacen.AgregarAula(cmbAula.Text))
                    {
                        int a2 = 0;
                        var aul = ControlAlmacen.Combos("call pAulas('"+cmbAula.Text+"',5)");
                        for (int i = 0; i < aul.Count; i++)
                        {
                            if (cmbAula.Text == aul[i].Nombre)
                                a2 = aul[i].ID;
                        }

                        if (ControlAlmacen.AgreagarPersonas(new Persona() { ID = txtNoControl.Text, Nombre = txtNombre.Text, Paterno = txtPaterno.Text, Materno = txtMaterno.Text }))
                        {
                            ControlAlmacen.SetPrestamo(new PrestamoHerramienta()
                            {
                                Persona = txtNoControl.Text,
                                Fecha = DateTime.Now.ToString("yyyy-MM-dd"),
                                Hora = DateTime.Now.ToString("HH:mm"),
                                Nota = "",
                                Aula = (a != 0) ? a.ToString() : a2.ToString(),
                                Materia = (m != 0) ? m.ToString() : m2.ToString(),
                            }, 1);
                            for (int i = 0; i < SetHerramienta.Count; i++)
                            {
                                ControlAlmacen.PrestarHerramienta(new PrestamoHerramientaDetalle() { ID = SetHerramienta[i].Codigo, Herramienta = SetHerramienta[i].Nombre }, 1);
                            }
                            if (MyConfig.Confirmacion)
                            {
                                frmMensaje M = new frmMensaje("Correcto", "El prestamo se realizo correctamente");
                                M.ShowDialog();
                            }
                            Cancelar();
                        }
                    }
                }
            }
        }
        private void Mensaje()
        {
            if (MyConfig.Error)
            {
                frmMensaje M = new frmMensaje("Error", "Favor de rellenar todos los campos correspondientes");
                M.ShowDialog();
            }
        }

        private void dgvPrestamo_DataSourceChanged(object sender, EventArgs e)
        {
            cmbAula.Enabled = false;
            cmbMateria.Enabled = false;
            for (int i = 0; i < dgvPrestamo.Rows.Count; i++)
            {
                string h = dgvPrestamo.Rows[i].Cells[1].Value.ToString();
                for (int p = 0; p < h.Length - 3; p++)
                {
                    if (h.Substring(p, 4) == "hdmi" || h.Substring(p, 4) == "HDMI")
                    {
                        cmbAula.Enabled = true;
                        cmbMateria.Enabled = true;
                        break;
                    }
                }
            }
        }
    }
}
