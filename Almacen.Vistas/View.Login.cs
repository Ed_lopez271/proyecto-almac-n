﻿using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Forms;
using Almacen.Entidades;
using Almacen.LogicaNegocios;

namespace Almacen.Vistas
{
    public partial class frmLogin : Form
    {
        private cAlmacen ControlAlmacen;
        public frmLogin()
        {
            InitializeComponent();
            ControlAlmacen = new cAlmacen();
        }

        private void txtUser_Enter(object sender, EventArgs e)
        {
            if (txtUser.Text == "PERSONAL")
            {
                txtUser.Text = "";
                txtUser.ForeColor = Color.Black;
            }
        }

        private void txtpass_Enter(object sender, EventArgs e)
        {
            if (txtpass.Text == "CONTRASEÑA")
            {
                txtpass.Text = "";
                txtpass.ForeColor = Color.Black;
                txtpass.UseSystemPasswordChar = true;   
            }
        }

        private void txtUser_Leave(object sender, EventArgs e)
        {
            if(txtUser.Text == "")
            {
                txtUser.Text = "PERSONAL";
                txtUser.ForeColor = Color.Silver;
            }
        }

        private void txtpass_Leave(object sender, EventArgs e)
        {
            if (txtpass.Text == "")
            {
                txtpass.UseSystemPasswordChar = false;
                txtpass.Text = "CONTRASEÑA";
                txtpass.ForeColor = Color.Silver;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void pnlTop_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ControlAlmacen.VerificarPersonal(txtUser.Text,txtpass.Text))
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                txtpass.Clear();
                txtUser.Clear();
                frmMensaje M = new frmMensaje("Error", "Usuario no valido para esta pestaña");
                M.ShowDialog();
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
