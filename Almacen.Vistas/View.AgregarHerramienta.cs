﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Almacen.Entidades;
using Almacen.LogicaNegocios;

namespace Almacen.Vistas
{
    public partial class frmAgregarHerramienta : Form
    {
        private cAlmacen ControlAlmacen;
        private List<Herramienta> Nuevas;
        private int index = 0;
        public frmAgregarHerramienta()
        {
            InitializeComponent();
            ControlAlmacen = new cAlmacen();
            Nuevas = new List<Herramienta>();
        }

        private void frmAgregarHerramienta_Load(object sender, EventArgs e)
        {
            Nuevas.Add(new Herramienta());
            Desactivar();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void pnlTop_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void lblAppTitle_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            AgregarLista();
            if (index != 0)
                index--;
            MostrarElemento();
            Desactivar();
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            if (AgregarLista())
            {
                index++;
                if (index == Nuevas.Count)
                    if ((!string.IsNullOrEmpty(txtCodigo.Text)) && (!string.IsNullOrEmpty(txtHerramienta.Text)))
                        Nuevas.Add(new Herramienta());
            }
            MostrarElemento();
            Desactivar();
        }

        private bool AgregarLista()
        {
            if ((!string.IsNullOrEmpty(txtCodigo.Text)) && (!string.IsNullOrEmpty(txtHerramienta.Text)))
            {
                Nuevas[index].Codigo = txtCodigo.Text;
                Nuevas[index].Nombre = txtHerramienta.Text;
                Nuevas[index].Prioridad = txtPrioridad.Text;
                Nuevas[index].Descripcion = txtDescripcion.Text;
                Nuevas[index].Estatus = txtEstatus.Text;
                Nuevas[index].Registro = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
                Nuevas[index].Disponibilidad = true;
                return true;
            }
            else
                return false;
        }
        private void MostrarElemento()
        {
            txtCodigo.Text = Nuevas[index].Codigo;
            txtHerramienta.Text = Nuevas[index].Nombre;
            txtPrioridad.Text = Nuevas[index].Prioridad;
            txtDescripcion.Text = Nuevas[index].Descripcion;
            txtEstatus.Text = Nuevas[index].Estatus;
        }
        private void Desactivar()
        {
            if (index == 0)
            {
                btnAnterior.BackColor = Color.DarkGray;
                btnAnterior.Enabled = false;
            }
            else
            {
                btnAnterior.BackColor = Color.Silver;
                btnAnterior.Enabled = true;
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarLista();
            int pos = 0;
            for (int i = 0; i < Nuevas.Count; i++)
            {
                if ((!string.IsNullOrEmpty(Nuevas[i].Codigo)) && (!string.IsNullOrEmpty(Nuevas[i].Nombre)))
                    if (!ControlAlmacen.SetHerramientas(Nuevas[i]))
                    {
                        pos = i;
                        i = Nuevas.Count;
                    }
            }
            if(pos != 0)
            {
                frmMensaje M = new frmMensaje("Error", "Se produjo un error al agregar la herramienta " + Nuevas[pos]);
                M.ShowDialog();
            }
            else
            {
                frmMensaje M = new frmMensaje("Correcto", "Todas las herramientas se agregaron correctamente");
                M.ShowDialog();
            }
        }
    }
}
