﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Almacen.Entidades;
using Almacen.LogicaNegocios;

namespace Almacen.Vistas
{
    public partial class frmPrestamo : Form
    {
        private cAlmacen ControlAlmacen;
        private Configuraciones Myconfig;
        private bool selectar = false;

        public frmPrestamo(Configuraciones Configuracion)
        {
            InitializeComponent();
            ControlAlmacen = new cAlmacen();
            this.Myconfig = Configuracion;
        }

        private void frmPrestamo_Load(object sender, EventArgs e)
        {
            dgvPrestamo.DataSource = ControlAlmacen.MostrarHistoria(new PrestamoHerramienta(), 4);
            OnlyOne();
        }

        private void btnHecho_Click(object sender, EventArgs e)
        {
            selectar = false;
            if (Myconfig.Verificacion)
            {
                frmMensaje M = new frmMensaje("Confirmar", "¿Esta seguro que desea recivir este prestamo?");
                DialogResult R = M.ShowDialog();
                if (R == DialogResult.OK)
                    TerminarPerstamo();
            }
            else
                TerminarPerstamo();
            selectar = true;
        }
        private void TerminarPerstamo()
        {
            var algo = dgvPrestamo.SelectedRows[0];
            int _id = int.Parse(algo.Cells[0].Value.ToString());
            ControlAlmacen.SetPrestamo(new PrestamoHerramienta() { ID = _id }, 3);
            List<PrestamoHerramientaDetalle> Herramientas = ControlAlmacen.GetDetalle(_id);
            for (int i = 0; i < Herramientas.Count; i++)
            {
                ControlAlmacen.PrestarHerramienta(new PrestamoHerramientaDetalle() { ID = Herramientas[i].ID }, 3);
            }
            dgvPrestamo.DataSource = null;
            dgvPrestamo.DataSource = ControlAlmacen.MostrarHistoria(new PrestamoHerramienta(), 4);
        }

        private void dgvPrestamo_SelectionChanged(object sender, EventArgs e)
        {
            if (selectar)
            {
                var algo = dgvPrestamo.SelectedRows[0];
                int _id = int.Parse(algo.Cells[0].Value.ToString());
                dgvDetalle.DataSource = null;
                dgvDetalle.DataSource = ControlAlmacen.GetDetalle(_id);
            }
        }

        private void frmPrestamo_Shown(object sender, EventArgs e)
        {
            selectar = true;
        }
        private void OnlyOne()
        {
            if (dgvPrestamo.Rows.Count == 1)
            {
                var algo = dgvPrestamo.Rows[0];
                int _id = int.Parse(algo.Cells[0].Value.ToString());
                dgvDetalle.DataSource = null;
                dgvDetalle.DataSource = ControlAlmacen.GetDetalle(_id);
            }
        }
    }
}
