﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Almacen.Entidades;
using Almacen.LogicaNegocios;

namespace Almacen.Vistas
{
    public partial class frmPrincipal : Form
    {
        private int notError = 0;
        private Form hijo;
        private Configuraciones MyConfig;
        private cAlmacen ControlAlmacen;

        public frmPrincipal()
        {
            InitializeComponent();
            ControlAlmacen = new cAlmacen();
        }
        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd,int wMsg, int wParam, int lParam);
        private void pnlBarraTop_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle,0x112,0xf012,0);
        }
        private void EvitarColapso(Form form)
        {
            if (hijo != null)
            {
                hijo.Close();
            }
            if(form != null)
            {
                MyConfig = ControlAlmacen.GetConfiguracion();
                hijo = form;
                hijo.TopLevel = false;
                hijo.FormBorderStyle = FormBorderStyle.None;
                hijo.Dock = DockStyle.Fill;

                this.pnlCuerpo.Controls.Add(hijo);
                this.pnlCuerpo.Tag = hijo;
                hijo.BringToFront();
                hijo.Show();
            }
        }

        private void btnHerramienta_Click(object sender, EventArgs e)
        {
            notError = 1;
            EvitarColapso(new frmHerramienta(MyConfig));
            lblAppTitle.Text = "Almacen - Prestamo de herramientas";
        }

        private void btnLaboratorio_Click(object sender, EventArgs e)
        {
            notError = 1;
            EvitarColapso(new frmLaboratorio(MyConfig));
            lblAppTitle.Text = "Almacen - Prestamo de laboratorios";
        }

        private void btnCurso_Click(object sender, EventArgs e)
        {
            notError = 1;
            EvitarColapso(new frmPrestamo(MyConfig));
            lblAppTitle.Text = "Almacen - Prestamos en curso";
        }

        private void btnHistorial_Click(object sender, EventArgs e)
        {
            notError = 1;
            frmLogin L = new frmLogin();
            if (MyConfig.Verificacion)
            {
                DialogResult R = L.ShowDialog();
                if (R == DialogResult.OK)
                    EvitarColapso(new frmInventario(false));
            }
            else
                EvitarColapso(new frmInventario(false));
            lblAppTitle.Text = "Almacen - Historial de prestamos";
        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
            notError = 1;
            frmLogin L = new frmLogin();
            if (MyConfig.Verificacion)
            {
                DialogResult R = L.ShowDialog();
                if (R == DialogResult.OK)
                    EvitarColapso(new frmInventario(true));
            }
            else
                EvitarColapso(new frmInventario(true));
            lblAppTitle.Text = "Almacen - Inventario";
        }

        private void btnConfiguracion_Click(object sender, EventArgs e)
        {
            notError = 1;
            frmLogin L = new frmLogin();
            if (MyConfig.Verificacion)
            {
                DialogResult R = L.ShowDialog();
                if (R == DialogResult.OK)
                    EvitarColapso(new frmConfiguracion(MyConfig));
            }
            else
                EvitarColapso(new frmConfiguracion(MyConfig));
            lblAppTitle.Text = "Almacen -  Configuracion";
        }

        private void imgLogo_Click(object sender, EventArgs e)
        {
            if (notError != 0)
            {
                EvitarColapso(null);
                lblAppTitle.Text = "Almacen";
            }
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            MyConfig = ControlAlmacen.GetConfiguracion();
        }
    }
}
