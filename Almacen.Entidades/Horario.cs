﻿namespace Almacen.Entidades
{
    public class Horario
    {
        public string Hora { get; set; }
        public string Lunes { get; set; }
        public string Martes { get; set; }
        public string Miercoles { get; set; }
        public string Jueves { get; set; }
        public string Viernes { get; set; }
        public string Sabado { get; set; }
    }

    public class MandarNuevo
    {
        public int Laboratorio { get; set; }
        public int Dia { get; set; }
        public int Hora { get; set; }
    }
}
