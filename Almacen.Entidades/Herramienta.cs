﻿namespace Almacen.Entidades
{
    public class Herramienta
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Prioridad { get; set; }
        public string Descripcion { get; set; }
        public string Estatus { get; set; }
        public string Registro { get; set; }
        public bool Disponibilidad { get; set; }
    }

    public class AntePrestamo
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }
    }
}
