﻿namespace Almacen.Entidades
{
    public class PrestamoHerramienta
    {
        public int ID { get; set; }
        public string Persona { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string Nota { get; set; }
        public bool Status { get; set; }
        public string Aula { get; set; }
        public string Materia { get; set; }
    }
    public class PrestamoHerramientaDetalle
    {
        public int Prestamo { get; set; }
        public string ID { get; set; }
        public string Herramienta { get; set; }
        public string Descripcion { get; set; }

    }
    public class PrestamoLaboratorio
    {
        public int ID { get; set; }
        public int Aula { get; set; }
        public int Dia { get; set; }
        public string Entrada { get; set; }
        public string Salida { get; set; }
        public string Responsable { get; set; }
    }
}
