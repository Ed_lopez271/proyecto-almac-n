﻿namespace Almacen.Entidades
{
    public class Persona
    {
        public string ID { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
    }
}
